# Final Lo Visto
Práctica final del curso 2020/21

# Entrega practica

##Datos

* Nombre: César Borao Moratinos
* Titulacion: DG ITT + IAA
* Despliegue (url): http://cborao.pythonanywhere.com/
* Video básico (url): https://youtu.be/Py7Nw62k-7E
* Video parte opcional (url): https://youtu.be/U9eqrIz5HUU

## Cuenta Admin Site

* admin/admin

## Cuentas usuarios

* edu/satenurjc
* carlos/satenurjc
* laura/satenurjc

## Resumen parte obligatoria

La aplicación ha sido diseñada poniendo especial atención a Bootstrap, para que sea responsiva.
La estructura de las plantillas es siempre la misma: una columna derecha donde arriba del todo tenemos la formulario de login/logout y debajo las funcionalidades del usuario (enlace a la pagina de usuario, aportes realizados...) si las hubiera.
En la coluna izquierda se muestra el contenido de los aportes y los comentarios.

Si un usuario realiza un aporte que ya estaba previamente en la aplicación, el aporte antiguo se elimina y se vuelve a crear como una aportación nueva (con los nuevos valores de título, descripción y usuario).

En el caso de introducir en la aportación una url que no sea válida o que conlleve a algún error, dicho aporte se ignora y no es creado.

## Lista partes opcionales
* Nombre parte: favicon.ico.
* Nombre parte: descargar comentarios en json.
* Nombre parte: Reconoce recursos de los 4 sitios propuestos (youtube, reddit, wikipedia y aemet)



