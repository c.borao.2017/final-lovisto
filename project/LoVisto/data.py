"""
César Borao Moratinos: data.py
En este módulo guardamos las plantillas que se utilizarán para formatear los datos de la
información extendida extraída de los recursos reconocidos y no reconocidos
"""

# Plantillas para recursos de Wikipedia
WIKI = """
    <div class="wikipedia">
        <p>
            Artículo Wikipedia: {titulo}
        </p>
        <p>  
            <img src="{imagen}">
        </p> 
        <p>
            {texto}
        </p>
        <p>Copyright Wikipedia.<a href="{url}">Artículo original</a>.
        </p>
    </div>
"""

# Plantillas para recursos de Aemet
CABECERA = """
    <div class="aemet">
        <p>Datos AEMET para {municipio} ({provincia})</p>
        <ul class="text-justify">"""

DAY = """
    <li>{fecha}. Temperatura: {tempMin}/{tempMax}, sensación: {sensMin}/{sensMax}, humedad: {humMin}/{humMax}.</li>"""

COPYRIGHT = """<ul><br>
            <p id=copy_aemet>{copy}<a href="http://{url}"> Página original en AEMET</a></p>
            </div>"""

# Plantillas para recursos de Youtube
VIDEO = """<div class="youtube">
                <p>Video YouTube: {titulo}</p>
                {video}
                <p>Autor: <a href="{link_autor}">{nombre_autor}</a>. <a href='https://www.youtube.es/watch?v={url}'>
                    Video en Youtube</a>
                </p>
           </div>"""

# Plantillas para recursos de Reddit
REDD_TEXT = """
<div class="reddit_text">
    <p>Nota Reddit: {titulo}</p>

    <p>{texto}</p>
    <p><a href="{url}">Publicado en {subreddit}</a>. Aprobación: {aprobacion}.</p>
</div>
"""

REDD_IMG = """
<div class="reddit_img" id="centrador">
    <p>Nota Reddit: {titulo}</p>
    <img src="{url}" class="img-fluid redd_image">
    <p><a href="{url}">Publicado en {subreddit}</a>. Aprobación: {aprobacion}.</p>
</div>
"""

# Plantillas para recursos no reconocidos
OG = """
<div class="og">
    <p>{titulo}</p>
    <p><img src="{imagen}" class="img-fluid"></p>
</div>
"""

TITLE = """
<div class="title_html">
    <p>{titulo}</p>
</div>
"""

NO_INFO = """
<div class="html">
    <p>Información extendida no disponible</p>
</div>
"""


