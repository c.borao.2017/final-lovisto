#!/usr/bin/python3

# Simple HTML parser for extract extended info
# César Borao Moratinos. 2021
# SAT subject (Universidad Rey Juan Carlos)

from html.parser import HTMLParser


class WebParser(HTMLParser):

    def __init__(self):
        HTMLParser.__init__(self)
        self.og_title = ""
        self.og_img = ""
        self.title = ""
        self.inHead = False
        self.inTitle = False
        self.is_og_title = False
        self.is_og_img = False
        self.og_data = {}
        self.title_data = {}

    def handle_starttag(self, tag, attributes):
        if tag == "head":
            self.inHead = True
        elif tag == "meta":
            for attr in attributes:
                if attr[0] == "content":
                    if self.is_og_title:
                        self.og_title = attr[1]
                        self.is_og_title = False
                    elif self.is_og_img:
                        self.og_img = attr[1]
                        self.is_og_img = False
                elif attr[0] == "property" and attr[1] == "og:title":
                    self.is_og_title = True
                elif attr[0] == "property" and attr[1] == "og:image":
                    self.is_og_img = True

        elif self.inHead and tag == "title":
            self.inTitle = True

    def handle_data(self, data):
        self.title = data

    def handle_endtag(self, tag):
        if tag == "title":
            self.title_data['title'] = self.title
            self.inTitle = False

        elif tag == "head":
            if self.og_title:
                self.og_data['og_title'] = self.og_title
            if self.og_img:
                self.og_data['og_img'] = self.og_img

            self.inHead = False
            self.og_title = ""
            self.og_img = ""


class WebContent:

    def __init__(self, stream):
        self.parser = WebParser()
        self.parser.feed(stream)

    def info(self):
        return self.parser.og_data, self.parser.title_data
