# Generated by Django 3.1.7 on 2021-05-28 07:10

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Resource',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=128)),
                ('url', models.CharField(max_length=128)),
                ('description', models.TextField()),
                ('date', models.DateTimeField(verbose_name='published')),
                ('info', models.TextField()),
            ],
        ),
    ]
