from django.db import models


class Content(models.Model):
    title = models.CharField(max_length=128)
    site = models.CharField(max_length=128)
    resource = models.CharField(max_length=128)
    description = models.TextField(blank=False)
    date = models.DateTimeField('published')
    info = models.TextField()
    user = models.CharField(max_length=200, default='None')
    comments = models.IntegerField(default=0)
    likes = models.IntegerField(default=0)
    dislikes = models.IntegerField(default=0)

    def __str__(self):
        return self.user + ": " + self.title


class Comment(models.Model):
    content = models.ForeignKey(Content, on_delete=models.CASCADE)
    title = models.CharField(max_length=200)
    body = models.TextField(blank=False)
    date = models.DateTimeField('published')
    user = models.CharField(max_length=200, default='None')

    def __str__(self):
        return self.content.title + ": " + self.title


class Vote(models.Model):
    TYPE = [
        (1, 'like'),
        (-1, 'dislike'),
    ]

    content = models.ForeignKey(Content, on_delete=models.CASCADE)
    user = models.CharField(max_length=200, default='None')
    decision = models.IntegerField(choices=TYPE)

    def __str__(self):
        return self.user + ": " + self.content.title

