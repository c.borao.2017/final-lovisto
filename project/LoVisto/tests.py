from django.contrib.auth.models import User
from django.views.decorators.csrf import csrf_exempt
from django.test import TestCase
from django.test import Client


class GetTests(TestCase):

    # Testeando un GET sobre la página principal
    @csrf_exempt
    def test_root_page(self):
        client = Client()
        response = client.get('/')
        self.assertEqual(response.status_code, 200)

        content = response.content.decode('utf-8')
        self.assertIn('Aporta, comenta y comparte contenido de internet.', content)

    # Testeando un GET de la página de información
    @csrf_exempt
    def test_info_page(self):
        client = Client()
        response = client.get('/info')
        self.assertEqual(response.status_code, 200)

        content = response.content.decode('utf-8')
        self.assertIn("Práctica final de la asignatura de Servicios y Aplicaciones Telemáticas (SAT).", content)

    # Testeando un GET de la página de usuario (estando logeado)
    @csrf_exempt
    def test_user_page(self):
        client = Client()

        user = User.objects.create(username='testuser')
        user.set_password('12345')
        user.save()

        logged_in = client.login(username='testuser', password='12345')
        self.assertTrue(logged_in)

        response = client.get('/user')
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn("Comentario", content)

    # Testeando un GET de la página de usuario (sin estar logeado)
    @csrf_exempt
    def test_user_page(self):
        client = Client()

        response = client.get('/user')
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn("You are not allowed to access this page. Please Login.", content)

    # Testeando un GET de la página de todos los aportes
    @csrf_exempt
    def test_all_page(self):
        client = Client()
        response = client.get('/all')
        self.assertTemplateUsed(response, "LoVisto/allpage.html")
        self.assertEqual(response.status_code, 200)

    # Testeando un GET de una página de un aporte
    @csrf_exempt
    def test_resource_page(self):

        # primero creamos el aporte
        client = Client()
        url = "http://www.aemet.es/es/eltiempo/prediccion/municipios/getafe-id28065"
        client.post('/', {'action': 'Add content','url': url, 'title': 'titulo',
                                     'description': 'El tiempo aemet'})

        # testamos el get a la página del aporte
        response2 = client.get('/resources/aemet/28065')
        self.assertTemplateUsed(response2, "LoVisto/resource.html")
        self.assertEqual(response2.status_code, 200)

    # Testeando GET del favicon
    @csrf_exempt
    def test_favicon(self):
        client = Client()
        response = client.get('/images/favicon.ico')
        self.assertEqual(response.status_code, 302)

    # Testeando GET del banner
    @csrf_exempt
    def test_favicon(self):
        client = Client()
        response = client.get('/images/banner.png')
        self.assertEqual(response.status_code, 302)

    # Testeando GET del archivo css del modo nocturno
    @csrf_exempt
    def test_dark_css(self):
        client = Client()
        response = client.get('/css/dark.css')
        self.assertEqual(response.status_code, 302)

    # Testeando GET del archivo css del modo normal
    @csrf_exempt
    def test_styles_css(self):
        client = Client()
        response = client.get('/css/styles.css')
        self.assertEqual(response.status_code, 302)


class PostTests(TestCase):

    # Testeando añadir contenido: nuevo aporte
    @csrf_exempt
    def test_root_newcontent(self):
        client = Client()
        url = "http://www.aemet.es/es/eltiempo/prediccion/municipios/getafe-id28065"
        response = client.post('/', {'action': 'Add content',
                                     'url': url, 'title': 'titulo',
                                     'description': 'El tiempo aemet'})

        self.assertEqual(response.status_code, 200)

        content = response.content.decode('utf-8')
        self.assertIn('El tiempo aemet', content)

    # Testeando dar un like en un aporte de "/"
    @csrf_exempt
    def test_root_like(self):
        client = Client()

        # creamos un usuario y nos logeamos
        user = User.objects.create(username='testuser')
        user.set_password('12345')
        user.save()

        logged_in = client.login(username='testuser', password='12345')
        self.assertTrue(logged_in)

        url = "http://www.aemet.es/es/eltiempo/prediccion/municipios/getafe-id28065"
        response = client.post('/', {'action': 'Add content',
                                     'url': url, 'title': 'titulo',
                                     'description': 'El tiempo aemet'})

        response2 = client.post('/', {'action': 'like', 'resource': 'resources/aemet/28065'})

        self.assertEqual(response2.status_code, 200)

    # Testeando dar un dislike en un aporte de "/"
    @csrf_exempt
    def test_root_dislike(self):
        client = Client()

        user = User.objects.create(username='testuser')
        user.set_password('12345')
        user.save()

        logged_in = client.login(username='testuser', password='12345')
        self.assertTrue(logged_in)

        url = "http://www.aemet.es/es/eltiempo/prediccion/municipios/getafe-id28065"
        client.post('/', {'action': 'Add content',
                                     'url': url, 'title': 'titulo',
                                     'description': 'El tiempo aemet'})

        response2 = client.post('/', {'action': 'dislike', 'resource': 'resources/aemet/28065'})

        self.assertEqual(response2.status_code, 200)

    # Testeando añadir comentario a un contenido
    @csrf_exempt
    def test_resource_newcomment(self):
        client = Client()

        # creamos un usuario y nos logeamos
        user = User.objects.create(username='testuser')
        user.set_password('12345')
        user.save()

        logged_in = client.login(username='testuser', password='12345')
        self.assertTrue(logged_in)

        # Primero, creamos un contenido
        url = "http://www.aemet.es/es/eltiempo/prediccion/municipios/getafe-id28065"
        response = client.post('/', {'action': 'Add content',
                                     'url': url, 'title': 'titulo',
                                     'description': 'El tiempo aemet'})

        self.assertEqual(response.status_code, 200)

        # Ponemos un nuevo comentario
        response2 = client.post('/resources/aemet/28065', {'action': 'Add comment',
                                'title': 'titulo',
                                'body': 'este es el cuerpo'})

        self.assertEqual(response2.status_code, 200)
        content = response2.content.decode('utf-8')
        self.assertIn('este es el cuerpo', content)

    # Testeando hacer login de un usuario
    @csrf_exempt
    def test_post_login(self):
        client = Client()
        response = client.post('/login_user', {'username': 'testuser',
                                               'password': '12345',
                                               'redirurl': '/'})
        self.assertRedirects(response, "/")

    # Testeando hacer logout de un usuario
    @csrf_exempt
    def test_post_logout(self):
        client = Client()
        response = client.post('/logout', {'redirurl': '/'})
        self.assertRedirects(response, "/")
