from django.urls import path, re_path
from . import views
from django.views.generic.base import RedirectView
from django.contrib.staticfiles.storage import staticfiles_storage

urlpatterns = [

    path("", views.index),
    path('login_user', views.login_user),
    path('logout', views.logout_view),
    path('info', views.app_info),
    path('user', views.user),
    path('all', views.all_resources),
    re_path(r'resources/(?P<site>.+)\/(?P<resource>.+)', views.get_resource),

    # Acceso a los archivos estáticos
    path('css/dark.css', RedirectView.as_view(url=staticfiles_storage.url("LoVisto/css/dark.css"))),
    path('css/styles.css', RedirectView.as_view(url=staticfiles_storage.url("LoVisto/css/styles.css"))),
    path('images/banner.png', RedirectView.as_view(url=staticfiles_storage.url("LoVisto/images/banner.png"))),
    path('favicon.ico', RedirectView.as_view(url=staticfiles_storage.url("LoVisto/images/favicon.ico"))),
]
