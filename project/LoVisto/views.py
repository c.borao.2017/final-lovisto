import re
import urllib
import urllib.error
import urllib.request

from django.contrib.auth import authenticate, login, logout
from django.shortcuts import redirect, render
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
from django.utils import timezone

from .models import Content, Comment, Vote
from . import data

# Parsers
from .reddit_parser import RedditParser
from .wiki_parser import Wiki
from .youtube_parser import Video
from .aemet_parser import Aemet
from .html_parser import WebContent

# Inicializamos variable global con el archivo css por defecto de nuestras páginas
css_file = "LoVisto/css/styles.css"


@csrf_exempt
def app_info(request):
    """
    Procesamos peticiones del resurso '/info'.
    Se trata de la página de información de nuestra aplicación.
    """
    global css_file

    # Si hemos pulsado el botón de "Cambiar modo" en esta página, establecemos el css
    # correspondiente
    if request.method == "POST":
        if request.POST['action'] == "Cambiar modo":
            if css_file == "LoVisto/css/dark.css":
                css_file = "LoVisto/css/styles.css"
            else:
                css_file = "LoVisto/css/dark.css"

    # Añadimos la información para los enlaces del footer y el resto de datos del contexto
    lastsites = Content.objects.all().order_by('date').reverse()[:3]
    context = {"css_file": css_file, "csrf_token": csrf_exempt, "page": "/info", "lastsites": lastsites}

    # Renderizamos la plantilla
    return render(request, 'LoVisto/info.html', context=context, status=200)


@csrf_exempt
def processWikiInfo(resource):
    """
    Extraemos la información extendida de un recurso reconocido de wikipedia.
    Utilizamos el wiki_parser.py para extraerla y componer la plantilla que irá
    guardada como un string en la base de datos
    """
    info = ""

    # Diferenciamos imagen y texto
    article = resource.split('/', maxsplit=1)[1]
    text_url = "https://es.wikipedia.org/w/api.php?action=query&format=xml&titles=" + article + "&prop=extracts&exintro&explaintext"
    img_url = "https://es.wikipedia.org/w/api.php?action=query&titles=" + article + "&prop=pageimages&format=json&pithumbsize=100"

    try:
        textStream = urllib.request.urlopen(text_url)
        imgStream = urllib.request.urlopen(img_url)

        wiki = Wiki(textStream, imgStream)
        txt, img = wiki.info()

        # Generamos el html a partir de los datos que nos da el wiki_parser
        info = data.WIKI.format(titulo=txt['titulo'],
                                texto=txt['texto'],
                                imagen=img['imagen'],
                                url="http://es.wikipedia.org/" + resource)

    except urllib.error.HTTPError as err:
        print("catched: " + str(type(err)))
        print('HTTPError: {}'.format(err.code))
        pass
    except urllib.error.URLError as err:
        print("catched: " + str(type(err)))
        print('HTTPError: {}'.format(err.code))
        pass

    return info


@csrf_exempt
def processAemetInfo(resource):
    """
    Extraemos la información extendida de un recurso reconocido de aemet.
    Utilizamos el aemet_parser.py para extraerla y componer la plantilla que irá
    guardada como string en la base de datos
    """
    info = ""

    # Accedemos al archivo XML del aporte y extraemos la información extendida
    resource_id = resource.split('-id', maxsplit=1)[1]
    url = "https://www.aemet.es/xml/municipios/localidad_" + resource_id + ".xml"

    try:
        xmlStream = urllib.request.urlopen(url)
        aemet = Aemet(xmlStream)
        general, days = aemet.predicciones()

        # Generamos el html a partir de los datos que nos da el aemet_parser
        info = data.CABECERA.format(municipio=general['municipio'],
                                    provincia=general['provincia'])

        for day in days:
            info = info + data.DAY.format(tempMin=day['tempMin'],
                                          tempMax=day['tempMax'],
                                          sensMin=day['sensMin'],
                                          sensMax=day['sensMax'],
                                          humMin=day['humMin'],
                                          humMax=day['humMax'],
                                          fecha=day['fecha'])

        info = info + data.COPYRIGHT.format(copy=general['copyright'],
                                            url="www.aemet.es/" + resource)

    except urllib.error.HTTPError as err:
        print("catched: " + str(type(err)))
        print('HTTPError: {}'.format(err.code))
        pass
    except urllib.error.URLError as err:
        print("catched: " + str(type(err)))
        print('HTTPError: {}'.format(err.code))
        pass

    return info


@csrf_exempt
def processVideoInfo(resource):
    """
    Extraemos la información extendida de un recurso reconocido de youtube.
    Utilizamos el youtube_parser.py para extraerla y componer la plantilla que irá
    guardada como string en la base de datos.
    """

    info = ""

    # Accedemos al archivo JSON del aporte y extraemos la información extendida
    resource = resource.split('v=', maxsplit=1)[1]

    try:
        url = "https://www.youtube.com/oembed?format=json&url=https://www.youtube.com/watch?v=" + resource
        jsonStream = urllib.request.urlopen(url)
        video = Video(jsonStream)
        information = video.info()

        # Generamos el html con la información extendida del vídeo
        info = data.VIDEO.format(titulo=information['titulo'],
                                 video=information['video'],
                                 nombre_autor=information['nombre_autor'],
                                 link_autor=information['link_autor'],
                                 url=resource)

    except urllib.error.HTTPError as err:
        print("catched: " + str(type(err)))
        print('HTTPError: {}'.format(err.code))
        pass
    except urllib.error.URLError as err:
        print("catched: " + str(type(err)))
        print('HTTPError: {}'.format(err.code))
        pass

    return info


@csrf_exempt
def processRedditInfo(resource):
    """
    Extraemos la información extendida de un recurso reconocido de reddit, que
    podrá contener texto o imagen. Utilizamos el reddit_parser.py para extraerla
    y componer la plantilla que irá guardada como string en la base de datos
    """

    info = ""

    # Recuperamos el subreddit y el id del comentario y con ello componemos la url del .json
    subreddit = resource.split('/')[1]
    resource_id = resource.split('/')[3]

    url = "https://www.reddit.com/r/" + subreddit + "/comments/" + resource_id + "/.json"
    # Abrimos el archivo json y lo parseamos
    try:
        jsonStreamReddit = urllib.request.urlopen(url)
        reddit = RedditParser(jsonStreamReddit)
        information = reddit.info()

        # Extraemos la url de la información para ver si se trata de un comentario de texto
        # o de una imagen
        url = information['url']
        pattern = re.compile("https://i.redd.it/.+")

        # componemos el html con la información extendida, diferenciando si es texto o imagen
        if pattern.match(url):
            info = data.REDD_IMG.format(titulo=information['titulo'],
                                        subreddit=information['subreddit'],
                                        url=url,
                                        aprobacion=information['aprobacion'])
        else:
            info = data.REDD_TEXT.format(titulo=information['titulo'],
                                         subreddit=information['subreddit'],
                                         texto=information['texto'],
                                         url=url,
                                         aprobacion=information['aprobacion'])

    except urllib.error.HTTPError as err:
        print("catched: " + str(type(err)))
        print('HTTPError: {}'.format(err.code))
        pass
    except urllib.error.URLError as err:
        print("catched: " + str(type(err)))
        print('HTTPError: {}'.format(err.code))
        pass

    return info


@csrf_exempt
def processPageInfo(og, title):
    """
    Extraemos la información HTML de un recurso no reconocido a partir de las propiedades OpenGraph
    y HTML, formateamos la información y la guardamos en la base de datos
    """
    # Si existen propiedades OpenGraph de título, imagen, o ambas
    if og != {}:
        if "og_title" in og:
            titulo = og['og_title']
        else:
            titulo = ""

        if "og_img" in og:
            imagen = og['og_img']
        else:
            imagen = ""
        info = data.OG.format(titulo=titulo, imagen=imagen)

    # Si no hay propiedades OG, pero sí tiene título
    elif title != {}:
        info = data.TITLE.format(titulo=title['title'])

    # Si no tiene nada de la información anterior
    else:
        info = data.NO_INFO.format()

    return info


@csrf_exempt
def index(request):
    """
    Procesa las peticiones al recurso raiz de la aplicacion.
    """
    username = request.user.username
    global css_file

    # Si se ha hecho un POST sobre el recurso raiz
    if request.method == "POST":

        # Si el POST es para añadir contenido
        if request.POST['action'] == "Add content":

            # Dividimos la url en sitio (site) y recurso (resource)
            url = request.POST['url']

            # Eliminamos el indicador de protocolo http:// si lo tuviera
            if url.startswith('https://') or url.startswith('http://'):
                url = url.split('//', maxsplit=1)[1]

            # Diseccionamos qué tipo de url tenemos
            if url == "":
                site = ""
                resource = ""
            elif url.find('/') == -1:
                site = url
                resource = ""
            else:
                site = url.split('/', maxsplit=1)[0]
                resource = url.split('/', maxsplit=1)[1]

            # Creamos una variable que nos indicará si es un recurso
            # reconocido, o no.
            recognized = False

            # Si la url viene de "es.wikipedia.org"
            if site == 'es.wikipedia.org':
                pattern = re.compile("wiki/.+")

                # Si es un formato de recurso conocido
                if pattern.match(resource):
                    try:
                        # Si alguien hace el mismo aporte, se elimina el anterior
                        resource_id = "resources/wikipedia/" + resource.split('/', maxsplit=1)[1]
                        content = Content.objects.get(resource=resource_id)
                        content.delete()

                    except Content.DoesNotExist:
                        pass

                    # Creamos el nuevo aporte
                    info = processWikiInfo(resource)
                    title = request.POST['title']

                    # Ponemos un titulo por defecto, en el caso de que no tenga
                    if title == "":
                        title = "Aporte sin titulo"

                    description = request.POST['description']
                    date = timezone.now()

                    new = Content(title=title, site=site, resource=resource_id,
                                  description=description, date=date, info=info, user=username)
                    new.save()
                    recognized = True

            # Si la url viene de "www.aemet.es"
            if site == 'www.aemet.es' or site == 'aemet.es':
                pattern = re.compile("es/eltiempo/prediccion/municipios/.+-id.+")

                # Si es un formato de recurso conocido
                if pattern.match(resource):
                    try:
                        # Si alguien hace el mismo aporte, se elimina el anterior
                        resource_id = "resources/aemet/" + resource.split('-id', maxsplit=1)[1]
                        content = Content.objects.get(resource=resource_id)
                        content.delete()

                    except Content.DoesNotExist:
                        pass

                    # Creamos el nuevo aporte
                    info = processAemetInfo(resource)
                    title = request.POST['title']

                    # Ponemos un titulo por defecto, en el caso de que no tenga
                    if title == "":
                        title = "Aporte sin titulo"

                    description = request.POST['description']
                    date = timezone.now()

                    new = Content(title=title, site=site, resource=resource_id,
                                  description=description, date=date, info=info, user=username)
                    new.save()
                    recognized = True

            # Si la url viene de "www.youtube.com"
            elif site == 'www.youtube.com' or site =='youtube.com':
                pattern = re.compile("watch\?v=.+")

                # Si es un formato de recurso conocido
                if pattern.match(resource):
                    try:
                        resource_id = "resources/youtube/" + resource.split('v=', maxsplit=1)[1]
                        content = Content.objects.get(resource=resource_id)
                        content.delete()

                    except Content.DoesNotExist:
                        pass

                    # Creamos el nuevo aporte
                    info = processVideoInfo(resource)
                    title = request.POST['title']
                    if title == "":
                        title = "Aporte sin titulo"

                    description = request.POST['description']
                    date = timezone.now()

                    c = Content(title=title, site=site, resource=resource_id,
                                description=description, date=date, info=info, user=username)
                    c.save()
                    recognized = True

            # Si la url viene de "www.reddit.com"
            elif site == 'www.reddit.com' or 'reddit.com':
                pattern = re.compile("r/.+/comments/.+/.+/")

                # Si es un formato de recurso conocido
                if pattern.match(resource):
                    try:
                        # Si alguien ya ha hecho el mismo aporte, se elimina.
                        resource_id = "resources/reddit/" + resource.split('/')[1] + "-" + resource.split('/')[3]
                        content = Content.objects.get(resource=resource_id)
                        content.delete()

                    except Content.DoesNotExist:
                        pass

                    # Creamos el nuevo aporte
                    info = processRedditInfo(resource)
                    title = request.POST['title']
                    if title == "":
                        title = "Aporte sin titulo"

                    description = request.POST['description']
                    date = timezone.now()

                    c = Content(title=title, site=site, resource=resource_id,
                                description=description, date=date, info=info, user=username)
                    c.save()
                    recognized = True

            # Si no ha coincidido con un recurso reconocido, extraemos las características HTML
            if not recognized:
                try:
                    html_stream = str(urllib.request.urlopen("https://" + site + "/" + resource).read())
                    page = WebContent(html_stream)
                    og, title = page.info()

                    try:
                        # Elegimos un nombre de recurso para el aporte
                        if resource == "":
                            resource = "resources/general/" + url
                        else:
                            resource = "resources/" + site + "/" + resource

                        content = Content.objects.get(resource=resource)
                        content.delete()

                    except Content.DoesNotExist:
                        pass

                    # Creamos el nuevo aporte
                    info = processPageInfo(og, title)
                    title = request.POST['title']
                    if title == "":
                        title = "Aporte sin titulo"

                    description = request.POST['description']
                    date = timezone.now()

                    c = Content(title=title, site=url, resource=resource,
                                description=description, date=date, info=info, user=username)
                    c.save()

                # Si hay un error en urlopen, simplemente no guarda el aporte
                except urllib.error.HTTPError as err:
                    print("catched: " + str(type(err)))
                    print('HTTPError: {}'.format(err.code))
                    pass
                except urllib.error.URLError as err:
                    print("catched: " + str(type(err)))
                    print('HTTPError: {}'.format(err.code))
                    pass

        # Si hemos pulsado el boton de "cambiar modo", cambiamos el css
        elif request.POST['action'] == "Cambiar modo":
            if css_file == "LoVisto/css/dark.css":
                css_file = "LoVisto/css/styles.css"
            else:
                css_file = "LoVisto/css/dark.css"

        # Si hemos votado "like" a una aportacion
        elif request.POST['action'] == "like":
            resource = request.POST['resource']
            content = Content.objects.get(resource=resource)
            liked_resource(content, username)

        # Si hemos votado "dislike" a una aportacion
        elif request.POST['action'] == "dislike":
            resource = request.POST['resource']
            content = Content.objects.get(resource=resource)
            disliked_resource(content, username)

            # dislike_button = "btn btn-success"
            # like_button = "btn btn-secondary"

    # Cuando hacemos un GET a "/" concatenando ?format=xml
    if request.GET.get('format') == "xml":
        contents = Content.objects.all().order_by('date').reverse()
        context = {"contents": contents}

        response = render(request, 'LoVisto/template.xml', context=context, status=200)
        response['Content-Type'] = 'application/xml'
        return response

    # Cuando hacemos un GET a "/" concatenando ?format=json
    if request.GET.get('format') == "json":
        contents = Content.objects.all().order_by('date').reverse()
        jsonFile = {}

        for i in range(0, len(contents), 1):
            content = contents[i]
            info = {'title': content.title, 'link': content.resource}
            jsonFile['Contenido ' + str(i)] = info

        return JsonResponse(jsonFile)

    # rellenamos el contexto y renderizamos la plantilla
    contents = Content.objects.all().order_by('date').reverse()[:10]

    # Si se ha hecho login, incluimos los contenidos asociados al usuario
    if request.user.is_authenticated:
        usercontents = Content.objects.all().order_by('date').filter(user=username).reverse()[:5]
    else:
        usercontents = ""

    lastsites = Content.objects.all().order_by('date').reverse()[:3]
    votes = Vote.objects.all()

    context = {"votes": votes, "css_file": css_file, "lastsites": lastsites,
               "contents": contents, "usercontents": usercontents,
               "csrf_token": csrf_exempt, "page": "/"}

    # Renderizamos la plantilla con los datos de contexto anteriores
    return render(request, 'LoVisto/index.html', context=context, status=200)


@csrf_exempt
def all_resources(request):
    """
    Método para responder peticiones referidas a la página de todas
    las aportaciones '/all'
    """
    global css_file

    # Si se ha pulsado el botón de cambiar modo, cambiamos el css que se sirve
    if request.method == "POST":
        if request.POST['action'] == "Cambiar modo":
            if css_file == "LoVisto/css/dark.css":
                css_file = "LoVisto/css/styles.css"
            else:
                css_file = "LoVisto/css/dark.css"

    # Recogemos los datos sobre todos los contenidos y la información para el footer
    contents = Content.objects.all().order_by('date').reverse()
    lastsites = Content.objects.all().order_by('date').reverse()[:3]

    context = {"css_file": css_file, "lastsites": lastsites,
               "contents": contents, "csrf_token": csrf_exempt, "page": "/all"}

    # Renderizamos la plantilla
    return render(request, 'LoVisto/allpage.html', context=context, status=200)


@csrf_exempt
def get_resource(request, site, resource):
    """
    Método para atender las peticiones a las páginas invididuales
    de los aportes
    """
    global css_file

    # Primero distinguimos de qué tipo de aporte se trata la petición y concatenamos
    # lo correspondiente para poder tener la ruta completa del aporte

    if site == "aemet":
        resource = "resources/aemet/" + resource
    elif site == "youtube":
        resource = "resources/youtube/" + resource
    elif site == "reddit":
        resource = "resources/reddit/" + resource
    elif site == "general":
        resource = "resources/general/" + resource
    else:
        resource = "resources/" + site + "/" + resource

    content = Content.objects.get(resource=resource)
    username = request.user.username

    # Si se ha hecho un POST sobre la página del aporte
    if request.method == "POST":

        # Si queremos añadir un comentario
        if request.POST['action'] == "Add comment":

            # Añadimos +1 comentario a la cuenta total de comentarios
            # que lleva el aporte
            content.comments += 1
            content.save()

            title = request.POST['title']
            if title == "":
                title = "Comentario sin titulo"

            body = request.POST['body']
            date = timezone.now()

            # Añadimos el comentario
            q = Comment(content=content, title=title, body=body, date=date, user=username)
            q.save()

        elif request.POST['action'] == "like":
            liked_resource(content, username)

        elif request.POST['action'] == "dislike":
            disliked_resource(content, username)

        # Si se ha pulsado el botón de cambiar modo, cambiamos el css que se sirve
        elif request.POST['action'] == "Cambiar modo":
            if css_file == "LoVisto/css/dark.css":
                css_file = "LoVisto/css/styles.css"
            else:
                css_file = "LoVisto/css/dark.css"

    # Cuando hacemos un GET a "/{recurso}" concatenando ?format=json
    if request.GET.get('format') == "json":
        jsonFile = {}

        comments = content.comment_set.all()
        for i in range(0, len(comments), 1):
            comment = comments[i]
            info = {'title': comment.title, 'body': comment.body}
            jsonFile['Comentario ' + str(i)] = info

        return JsonResponse(jsonFile)

    lastsites = Content.objects.all().order_by('date').reverse()[:3]
    votes = Vote.objects.all()
    context = {"votes": votes, "css_file": css_file, "lastsites": lastsites, "content": content, "csrf_token": csrf_exempt, "page": "/" + resource}

    return render(request, 'LoVisto/resource.html', context=context, status=200)


@csrf_exempt
def liked_resource(content, username):
    """
    Método para votar como 'like' una aportación para un usuario concreto
    """
    try:
        vote = Vote.objects.get(content=content, user=username)

        if vote.decision != 1:
            vote.decision = 1
            content.likes += 1

            if content.dislikes > 0:
                content.dislikes -= 1
            vote.save()

    except Vote.DoesNotExist:
        decision = 1
        content.likes += 1
        vote = Vote(content=content, user=username, decision=decision)

    content.save()
    vote.save()


@csrf_exempt
def disliked_resource(content, username):
    """
       Método para votar como 'dislike' una aportación para un usuario concreto
    """
    try:
        vote = Vote.objects.get(content=content, user=username)
        if vote.decision != -1:
            vote.decision = -1
            content.dislikes += 1

            if content.likes > 0:
                content.likes -= 1
            vote.save()

    except Vote.DoesNotExist:
        decision = -1
        content.dislikes += 1
        vote = Vote(content=content, user=username, decision=decision)

    content.save()
    vote.save()


@csrf_exempt
def user(request):
    """
    Atiende peticiones para el recurso "/user" correspondiente a la página
    del usuario
    """
    global css_file

    # Si se pulsa el botón de cambiar modo, se cambia el css que se sirve
    if request.method == "POST":
        if request.POST['action'] == "Cambiar modo":
            if css_file == "LoVisto/css/dark.css":
                css_file = "LoVisto/css/styles.css"
            else:
                css_file = "LoVisto/css/dark.css"

    # Si el usuario es autenticado, se muestran sus aportaciones, comentarios y votaciones personales
    if request.user.is_authenticated:
        username = request.user.username
        usercontents = Content.objects.all().order_by('date').filter(user=username)
        usercomments = Comment.objects.all().filter(user=username)
        uservotes = Vote.objects.all().filter(user=username)

    else:
        usercontents = ""
        usercomments = ""
        uservotes = ""

    lastsites = Content.objects.all().order_by('date').reverse()[:3]
    context = {"css_file": css_file, "lastsites": lastsites, "uservotes": uservotes,
               "usercomments": usercomments, "usercontents": usercontents, "csrf_token": csrf_exempt, "page": "/user"}

    return render(request, 'LoVisto/userpage.html', context=context, status=200)


@csrf_exempt
def login_user(request):
    """
    Atiende las peticiones de login, y redirige a la página desde donde
    se realizó la petición
    """
    redirurl = request.POST['redirurl']
    username = request.POST['username']
    password = request.POST['password']
    user_auth = authenticate(request, username=username, password=password)

    if user_auth is not None:
        login(request, user_auth)

    return redirect(redirurl)


@csrf_exempt
def logout_view(request):
    """
    Atiende las peticiones para hacer logout, y redirige a la página desde donde
    se realizó la petición
    """
    redirurl = request.POST['redirurl']
    logout(request)
    return redirect(redirurl)
