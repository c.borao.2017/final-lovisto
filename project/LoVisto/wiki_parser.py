#!/usr/bin/python3

# Simple parser for Wikipedia content
# César Borao Moratinos. 2021
# SAT subject (Universidad Rey Juan Carlos)
import json
from xml.sax.handler import ContentHandler
from xml.sax import make_parser


class WikiTextHandler(ContentHandler):

    def __init__(self):
        self.text = ""
        self.title = ""
        self.content = ""
        self.idx = ""
        self.inContent = False
        self.text_info = {}

    def startElement(self, name, attrs):

        if name == 'extract':
            self.inContent = True
        if name == 'page':
            self.title = attrs.get('title')
            self.idx = attrs.get('_idx')

    def endElement(self, name):

        if name == 'extract':
            self.text_info['texto'] = self.content
            self.inContent = True

        if name == 'page':
            self.text_info['titulo'] = self.title
            self.text_info['idx'] = self.idx

    def characters(self, chars):
        if self.inContent:
            if len(chars) < 400:
                self.content = self.content + chars
            else:
                self.content = self.content + chars[:400]


class Wiki:
    img_info = {}

    def __init__(self, txtStream, imgStream):
        self.parser = make_parser()
        self.handler = WikiTextHandler()
        self.parser.setContentHandler(self.handler)
        self.parser.parse(txtStream)

        id_article = self.handler.text_info['idx']

        info = json.load(imgStream)

        self.WikiImgParser(info, id_article)

    def WikiImgParser(self, info, article):

        if info['query']['pages'][article].get('thumbnail'):
            self.img_info['imagen'] = info['query']['pages'][article]['thumbnail']['source']
        else:
            self.img_info['imagen'] = ""
    def info(self):
        return self.handler.text_info, self.img_info
