#!/usr/bin/python3

# Simple Youtube parser for extract extended info
# César Borao Moratinos. 2021
# SAT subject (Universidad Rey Juan Carlos)

import json


class Video:
    video_info = {}

    def parser(self, info):
        self.video_info['titulo'] = info['title']
        self.video_info['nombre_autor'] = info['author_name']
        self.video_info['link_autor'] = info['author_url']
        self.video_info['video'] = info['html']

    def __init__(self, stream):
        info = json.load(stream)
        self.parser(info)

    def info(self):
        return self.video_info
